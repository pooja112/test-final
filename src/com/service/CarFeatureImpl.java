package com.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.test.CarFactory;

@Service("Carfeature")
public class CarFeatureImpl implements CarFeature {

HashMap<String,String> result=null;	
	
	public HashMap<String,String> getCarFeature(String carName) {
		 CarFactory cf=new CarFactory();
		 if(carName.equalsIgnoreCase("Ecosport"))
		 {
			 result=cf.ecosportFeature();
		 }
		 else  if(carName.equalsIgnoreCase("Figo"))
		 {
			 result=cf.figoFeature();
		 }
		 else  if(carName.equalsIgnoreCase("Endeavour"))
		 {
			 result=cf.endeavourFeature();
		 }
		 else  if(carName.equalsIgnoreCase("Mondeo"))
		 {
			 result=cf.mondeoFeature();
		 }
		 
		 return result;
		 
		 
		
	}

}
