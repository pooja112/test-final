package com.test;

public interface Car {
	
	public String price();
	public String color();
	public String dieselOrPetrol();

}
