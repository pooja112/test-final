package com.test;

import java.util.HashMap;

public class CarFactory {

	private Car ecosport;
	private Car figo;
	private Car endeavour;
	private Car mondeo;
	HashMap<String, String> feature=new HashMap<>();
	public CarFactory()
	{
		ecosport=new Ecosport();
		figo= new Figo();
		endeavour= new Endeavour();
		mondeo =new Mondeo();
	}
	
	public HashMap<String, String> ecosportFeature()
	{
		
		feature.put("price", ecosport.price());
		feature.put("color",ecosport.color());
		feature.put("fueltype", ecosport.dieselOrPetrol());
		return feature;
	}
	public HashMap<String, String> figoFeature()
	{
		
		feature.put("price", figo.price());
		feature.put("color",figo.color());
		feature.put("fueltype", figo.dieselOrPetrol());
		return feature;
	}
	public HashMap<String, String> endeavourFeature()
	{
		
		feature.put("price", endeavour.price());
		feature.put("color",endeavour.color());
		feature.put("fueltype", endeavour.dieselOrPetrol());
		return feature;
	}
	public HashMap<String, String> mondeoFeature()
	{
		
		feature.put("price", mondeo.price());
		feature.put("color",mondeo.color());
		feature.put("fueltype", mondeo.dieselOrPetrol());
		return feature;
	}
}
