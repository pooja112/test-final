package com.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.service.CarClientInterface;

public class CarFeatureServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// WeatherClient clientContext;
	CarClientInterface client;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ApplicationContext ac = null;

		try {
			ac = new ClassPathXmlApplicationContext("Car-Feature-Spring-Flow.xml");

		} catch (Exception e) {

			ac = new ClassPathXmlApplicationContext("/WEB-INF/Car-Feature-Spring-Flow.xml");

		}

		this.client = (CarClientInterface) ac.getBean("carClient");

		// this.clientContext =
		// (WeatherClient)ac.getBean("weatherClientContext");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// read form fields

		String carName = request.getParameter("carName");

		HashMap<String, String> result = client.invoke(carName);

		for (Map.Entry<String, String> entry : result.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();

			/*
			 * request.setAttribute("result",result);
			 * //request.getRequestDispatcher("/WEB-INF/Result.jsp").forward(
			 * request, response);
			 */

			response.getWriter()
					.write("<html><body> <b> " + " " + key + "   : " + value + "</b><br/></body></html>");
		}
	}
}
